# Simple Fortune File REST API

This is only intended as a simple demo of creating a Spring Boot project for a 
REST API. It reads a bunch of Unix "fortune" formatted files and turns them into
a set of quotes that can be returned randomly as JSON responses.

Initially there's only 2 fortune files in the resource bundle:
* debian - the default set of Debian fortunes from the Stretch release
* bone-wisdom - my own compiled list of quotes from the Bone comic series, by Jeff Smith (go read the comics, they're fun)

Feel free to poke about at it if you want.