package uk.co.projectarc.fortune.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import lombok.extern.slf4j.Slf4j;

/**
 * Response adapter to add a "X-FortuneVersion" header to every response from
 * this API. Adds an HTTP header to avoid needing to know anything about the
 * payload/response types or data.
 */
@ControllerAdvice
@Slf4j
public class FortuneVersionAdapter implements ResponseBodyAdvice<Object> {

	@Value("${fortune.apiVersion:1.0.0}")
	private String version;
	
	@Value("${fortune.versionHeader:X-FortuneVersion}")
	private String versionHeader;
	
	@Override
	public boolean supports(
		MethodParameter returnType,
		Class<? extends HttpMessageConverter<?>> converterType) {
		
		return true;
	}

	@Override
	public Object beforeBodyWrite(
		Object body,
		MethodParameter returnType,
		MediaType selectedContentType,
		Class<? extends HttpMessageConverter<?>> selectedConverterType,
		ServerHttpRequest request,
		ServerHttpResponse response) {

		log.debug("adding a version attribute: "+version);
		response.getHeaders().add(versionHeader, version);
		
		return body;
	}
}
