package uk.co.projectarc.fortune.controller;

import java.util.NoSuchElementException;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Adds global exception handling to the whole API
 */
@ControllerAdvice
public class FortuneExceptionHandler {

	@ResponseBody
	@ExceptionHandler(NoSuchElementException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String notFoundHandler(NoSuchElementException ex) {
		return "Sorry mate, no call for that one";
	}

	@ResponseBody
	@ExceptionHandler(NumberFormatException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	String badIdhandler(NumberFormatException ex) {
		return "That looked like a free man, not a number";
	}

	@ResponseBody
	@ExceptionHandler(ServletException.class)
	@ResponseStatus(HttpStatus.TOO_MANY_REQUESTS)
	String servletFailHandler(ServletException ex) {
		return ex.getMessage();
	}
}
