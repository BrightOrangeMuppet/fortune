package uk.co.projectarc.fortune.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import uk.co.projectarc.fortune.data.Fortune;
import uk.co.projectarc.fortune.repo.FortuneStore;

/**
 * Main read-only REST controller. Not HATEOAS, just basic get one/all stuff.
 */
@Api(description="A simple Spring Boot REST-ish service which returns random JSON formatted fortune cookies taken from whatever Unix style fortune bundles are configured.")
@Controller
public class FortuneController {

	@Autowired
	private FortuneStore fortunes;
	
	@Value("${fortune.defaultBundle:default}")
	private String defaultBundle;
	
	public FortuneController() {}
	
	@ApiOperation(value = "View all the fortunes for the specified bundle name.")
	@GetMapping(value= {"/fortunes", "/fortunes/{bundle}"})
	@ResponseBody
	public List<Fortune> getFortunesForBundle(
		@PathVariable(name="bundle", required=false) String bundle) {

		return fortunes.getFortunesForBundle(bundle != null ? bundle : defaultBundle);
	}
	
	@ApiOperation(value = "View a specific fortune from its ID")
	@GetMapping("/fortune/byId/{id}")
	@ResponseBody
	public Fortune getFortuneById(
		@PathVariable("id") Long id,
		@RequestParam(value = "rep", required = false) String rep) {

		return fortunes.findById(id).orElseThrow();
	}
	
	@ApiOperation(value = "View a random fortune taken from a specific bundle")
	@GetMapping("/fortune/byBundle/{bundle}")
	@ResponseBody
	public Fortune getFortuneByBundle(
		@PathVariable("bundle") String bundle,
		@RequestParam(value = "rep", required = false) String rep) {

		return fortunes.getFortuneFromBundle(bundle).orElseThrow();
	}
	
	@ApiOperation(value = "View a random fortune taken from all bundles")
	@GetMapping("/fortune")
	@ResponseBody
	public Fortune getFortune() {
		return fortunes.getRandom().orElseThrow();
	}
	
	@ApiOperation(value = "View a random fortune taken from all bundles, not as JSON format, but raw Unix fortune format")
	@GetMapping(value = "/fortune", params="rep=raw")
	@ResponseBody
	public String getRawFortune(@RequestParam(value = "rep") String rep) {
		return fortunes.getRandom().orElseThrow().getText();
	}
	
	@ApiOperation(value = "View a list of all available bundles")
	@GetMapping("/bundles")
	@ResponseBody
	public List<String> getBundles() {

		return fortunes.getBundles();
	}
}
