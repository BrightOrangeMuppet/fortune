package uk.co.projectarc.fortune.config;

import java.io.IOException;
import java.util.Date;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class FortuneConfig {

	@Value("${fortune.rate:0.5}")
	private double perSecondRate;

	@Bean
	public FilterRegistrationBean<Filter> initRateLimiter() {

		var bean = new FilterRegistrationBean<>();
		bean.addUrlPatterns("/fortune/*");

		bean.setFilter(new Filter() {

			private long lastTime;

			@Override
			public void doFilter(
				ServletRequest request,
				ServletResponse response,
				FilterChain chain)
				throws IOException, ServletException {

				var now = new Date().getTime();
				if (now - lastTime > 1000.0 / perSecondRate) {
					lastTime = now;
					chain.doFilter(request, response);
				} else {
					log.warn("Rejecting a request due to rate limiting.");
					var res = (HttpServletResponse)response;
					res.sendError(HttpStatus.TOO_MANY_REQUESTS.value(), "too fast!");
				}
			}

			@Override
			public void init(FilterConfig filterConfig) throws ServletException {
				lastTime = new Date().getTime();
			}

			@Override
			public void destroy() {}
		});

		return bean;
	}
}
