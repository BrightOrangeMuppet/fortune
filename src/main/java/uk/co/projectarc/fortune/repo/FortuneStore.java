package uk.co.projectarc.fortune.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import uk.co.projectarc.fortune.data.Fortune;

/**
 * JPA interface onto the underlying fortune repository
 */
public interface FortuneStore extends JpaRepository<Fortune, Long> {

    @Query("SELECT f FROM Fortune f WHERE f.bundle=(:bundle)")
    List<Fortune> getFortunesForBundle(String bundle);

	Fortune getFortunesById(String id);

    @Query("SELECT distinct f.bundle FROM Fortune f")
	List<String> getBundles();

    // native Hibernate/H2 query to get access to "limit" and "random" 
    @Query(value="SELECT * FROM Fortune f ORDER BY rand() fetch first row only", nativeQuery = true)
	Optional<Fortune> getRandom();

    // native Hibernate/H2 query to get access to "limit" and "random" 
    @Query(value="SELECT * FROM Fortune f where f.bundle=(:bundle) ORDER BY rand() fetch first row only", nativeQuery = true)
	Optional<Fortune> getFortuneFromBundle(String bundle);

}