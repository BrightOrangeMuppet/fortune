package uk.co.projectarc.fortune.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import uk.co.projectarc.fortune.repo.FortuneStore;

/**
 * Reads all the fortune files on the the resource path specified in the
 * property "fortune.path" (or in resources/fortunes), parsing each fortune
 * and storing them in the fortune repo.
 * 
 *  Reading happens on each app startup. Any unreadable and unparseable files
 *  are skipped. If no fortune files are found/parsed, a couple of default
 *  fortunes are added to the repo for good measure.
 */
@Service
@Slf4j
public class FortuneReader {

	public FortuneReader(
		@Value("${fortune.path:classpath:fortunes/*}") String resourcePath, 
		FortuneStore repo) {
		
		try {
		    ResourcePatternResolver loader = new PathMatchingResourcePatternResolver();
			for (Resource r : loader.getResources(resourcePath)) {
				readFileIntoRepo(r, repo);
			}
		} catch (IOException e1) {
			log.warn("No fortune files found on "+resourcePath+", using minimal defaults");
			loadDefaultFortunes(repo);
			return;
		}
	}
	
	private void loadDefaultFortunes(FortuneStore repo) {
		repo.save(new Fortune("default", "Put some fortune files in the correct directory"));
		repo.save(new Fortune("default", "Y'owd fool you"));
	}

	/**
	 * Reads a resource file in standard Unix fortunes format and saves each
	 * fortune into the fortune repo. All \n and \t characters are preserved
	 * as in the original file.
	 * @param fortunesFile the resource file to be read
	 * @param repo the repo to store the fortunes in
	 */
	private void readFileIntoRepo(Resource fortunesFile, FortuneStore repo) {
		
		String bundle = fortunesFile.getFilename();
		
		try (BufferedReader br = new BufferedReader(new FileReader(fortunesFile.getFile()))) {
			StringBuilder rawFortune = new StringBuilder();
		    String line;
		    while ((line = br.readLine()) != null) {
		    	if (line.startsWith("%")) {
		    		repo.save(new Fortune(bundle, rawFortune.toString()));
		    		rawFortune.setLength(0);
		    	} else {
		    		rawFortune.append(line).append("\n");
		    	}
		    }
		    
		    if (rawFortune.length() > 0) {
	    		repo.save(new Fortune(bundle, rawFortune.toString()));
		    }
		} catch (IOException e) {
			log.warn("failed to read "+fortunesFile.getFilename()+", error was: "+e.getMessage());
		}
		
	}
}
